package de.sschleis.locals;

import java.util.Locale;

/**
 * User: sebastian
 * Date: 04.07.13
 * Time: 23:53
 */
public class LocalsTwo {

    public static void main(String[] args) {
        LocalsTwo localsOne = new LocalsTwo();
        localsOne.run();
    }

    private static void run() {
        String[] locals = Locale.getISOCountries();
        Locale definedLocale = Locale.CHINESE;
        int counter = 0;
        for (String countryCode : locals) {
            Locale locale = new Locale("", countryCode);
            System.out.println("CountryCode = " + locale.getCountry() + ", CountryCode3 = " + locale.getISO3Country() + ", CountryName = " + locale.getDisplayCountry(definedLocale));
            counter++;
        }
        System.out.println("Done " + counter + " Countries");
    }
}
